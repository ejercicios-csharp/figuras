namespace figura
{
    public class Figura
    {
        public Figura() { }

        public virtual float Perimetro()
        {
            return 0;
        }

        public virtual float Area()
        {
            return 0;
        }
    }
}
