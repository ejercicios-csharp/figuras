namespace figura
{
    public class Circulo : Figura
    {
        public float radio { get; set; }

        public Circulo(float radio)
        {
            this.radio = radio;
        }
        
        //Multiplica 2 por el radio y por PI, y lo devuelve
        public override float Perimetro() 
        {
            return (float)(2 * radio * Math.PI);
        }

        //Hace el cuadrado del radio, lo multiplica por PI y lo devuelve
        public override float Area()
        {
            return (float)(radio * radio * Math.PI);
        }

    }
}
