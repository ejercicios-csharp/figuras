namespace figura
{
    public class Rectangulo : Figura
    {
        public float lado1 { get; set; }
        public float lado2 { get; set; }

        public Rectangulo(float lado1, float lado2)
        {
            this.lado1 = lado1;
            this.lado2 = lado2;
        }

        //Multiplica el lado1 por 2 y el lado2 por 2, lo suma, y lo devuelve
        public override float Perimetro()
        {
            return (float)(2 * lado1 + 2 * lado2);
        }

        //Multiplica el lado1 por lado2 y lo devuelve
        public override float Area()
        {
            return (float)(lado1 * lado2);
        }

    }
}
