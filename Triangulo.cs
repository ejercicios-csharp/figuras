namespace figura
{
    public class Triangulo : Figura
    {
        public float base1 {  get; set; }
        public float altura { get; set; }

        public Triangulo(float base1, float altura)
        {
            this.base1 = base1;
            this.altura = altura;
        }

        //Calcula la hipotenusa y se la suma a la altura y la base, y lo devuelve
        public override float Perimetro()
        {
            float hipotenusa = (float)(Math.Sqrt(Math.Pow(base1, 2) + Math.Pow(altura, 2)));
            return (float)(hipotenusa + altura + base1);
        }

        //Multiplica la base por la altura, lo divide entre 2, y lo devuelve
        public override float Area()
        {
            return (float)(base1 * altura / 2);
        }
    }
}
