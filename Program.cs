using System;
using System.IO;
using System.Net;

namespace figura
{
    public class Program
    {
        public static void Main()
        {
            int opcion;

            do
            {
                Console.WriteLine("0 - Salir");
                Console.WriteLine("1 - Rectangulo");
                Console.WriteLine("2 - Circulo");
                Console.WriteLine("3 - Triangulo");

                Console.Write("Introduzca su opcion: ");
                opcion = int.TryParse(Console.ReadLine(), out opcion) ? opcion : 4; //Pide un numero, si no es numero devuelve 4
                switch (opcion)
                {
                    case 1:
                        crearRectangulo();
                        continuar();
                        break;
                    case 2:
                        crearCirculo();
                        continuar();
                        break;
                    case 3:
                        crearTriangulo();
                        continuar();
                        break;
                    case 0:
                        break;
                    default:
                        Console.WriteLine("Introduzca una opcion valida");
                        break;
                }
            } while (opcion != 0);
        }

        //Limpia la consola
        public static void continuar()
        {
            Console.WriteLine("Pulsa INTRO para continuar...");
            Console.ReadLine();
            Console.Clear();
        }

        //Crea una figura rectangulo y muestra los valores de area y perimetro
        public static void crearRectangulo()
        {
            float lado1 = pideFloat("Lado 1: ");
            float lado2 = pideFloat("Lado 2: ");

            Figura figura = new Rectangulo(lado1, lado2);

            Console.WriteLine("Area: " + figura.Area());
            Console.WriteLine("Perimetro: " + figura.Perimetro());
        }

        //Crea una figura circulo y muestra los valores de area y perimetro
        public static void crearCirculo()
        {
            float radio = pideFloat("Radio: ");

            Figura figura = new Circulo(radio);

            Console.WriteLine("Area: " + figura.Area());
            Console.WriteLine("Perimetro: " + figura.Perimetro());
        }

        //Crea una figura triangulo y muestra los valores de area y perimetro
        public static void crearTriangulo()
        {
            float base1 = pideFloat("Base: ");
            float altura = pideFloat("Altura: ");

            Figura figura = new Triangulo(base1, altura);

            Console.WriteLine("Area: " + figura.Area());
            Console.WriteLine("Perimetro: " + figura.Perimetro());
        }

        //Pide un valor por teclado y lo convierte a float, si no puede, lo pide otra vez
        public static float pideFloat(string linea)
        {
            float num;

            do
            {
                Console.Write(linea);
                num = float.TryParse(Console.ReadLine(), out num) ? num : 0;
            } while (num <= 0);
            
            return num;
        }
    }
}
